import static org.junit.jupiter.api.Assertions.*;

import java.util.Scanner;

import org.junit.jupiter.api.Test;

class LibraryTesting {

	@Test
	void testReadInputFile() {
		Library library = new Library();
		library.readInputFile();
		library.displayCollection();
		assertEquals("Should be a game of thrones", "A Game of Thrones",library. materials[4].getTitle());
	}
	
	void testDisplayCollection() {
		Library library = new Library();
		library.readInputFile();
		library.displayCollection();
	}
	
	void testCheckOutMaterial() {
		Library library = new Library();
		library.readInputFile();
		library.displayCollection();
		library.checkOutMaterial();
		Scanner scan = new Scanner(System.in);
		String callNum = scan.nextLine();
	}

}
