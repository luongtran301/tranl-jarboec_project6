import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Library implements ILibrary {

	protected Material materials[];
	protected Scanner scanner;

	/**
	 * Prints out the greeting for the user
	 */
	public void greeting() {
		System.out.println("\n------------- Menu -------------" + "\n1) Display collection" + "\n2) Check out materials"
				+ "\n3) Quit" + "\n--------------------------------" + "\nPlease choose an option: ");
	}

	/**
	 * Reads the titles that the "library" stores
	 */
	public void readInputFile() {
		try {
			scanner = new Scanner(new File("sourceFile.txt"));
			int numOfBooks = Integer.parseInt(scanner.next());
			int numOfPeriodicals = Integer.parseInt(scanner.next());
			materials = new Material[(numOfBooks + numOfPeriodicals)];
			int i = 0;
			while (scanner.hasNext()) {
				String[] item = (scanner.nextLine()).split(",");
				if (item[0].equals("B")) {
					materials[i] = new Book(item[1], item[2], item[3], item[4]);
					i++;
				} else if (item[0].equals("P")) {
					materials[i] = new Periodical(item[1], item[2], item[3], item[4], item[5]);
					i++;
					// hello
				}
			}
		} catch (

		FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("File not found");
		}
	}
	
	/**
	 * Prints out the entire collection
	 */
	public void displayCollection() {
		for (int i = 0; i < materials.length; i++) {
			System.out.println(materials[i]);
		}
	}
	
	/**
	 * "Checks out" the material from the library
	 */
	public void checkOutMaterial(){
		System.out.println("Enter the call number: ");
		Scanner scan = new Scanner(System.in);
		String callNum = scan.nextLine();
		System.out.println(callNum);
		Material desiredMaterial = null;
		int itemIndex = -1; 
		for(int i = 0; i < materials.length; i++){
			if((materials[i].getCallNumber()).equals(callNum)){
			itemIndex = i;
			desiredMaterial = materials[i];
		}
		}
		try {
		if(itemIndex == -1) {
			throw new InvalidInputException();
		}
		}
		catch(InvalidInputException e) {
			System.out.println("Material not avaliable");
			greeting();
		}
		
		if(desiredMaterial.isCheckOut() == true) {
				System.out.println("This material is not avaliable!");
				greeting();
			}
		else {
				desiredMaterial.setIsCheckOut();
				desiredMaterial.setCheckOutDate();
				desiredMaterial.setDueDate();
				System.out.println(desiredMaterial.toString());
			}
		


	}


}
