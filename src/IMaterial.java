
public interface IMaterial {
	
	/**
	 * this method return the title of the material
	 * @return String
	 */
	public String getTitle();
	
	/**
	 * this method return the call number of the material
	 * @return String
	 */
	public String getCallNumber();
	
	/**
	 * this method return if the material is checked out or not
	 * @return boolean
	 */
	public boolean isCheckOut();
	
	/**
	 * this method would set the check out date of the material to the current date
	 */
	public void setCheckOutDate();
	
	/**
	 * this method would return the check out date of the material
	 * @return String
	 */
	public String getCheckOutDate();

	/**
	 * this is an abstract method which set the due date for each type of material
	 * each material would have its own specification on due date
	 */
	public abstract void setDueDate();
	
	/**
	 * this method return the due date fo the material
	 * @return String
	 */
	public String getDueDate();
	
	/**
	 * this method would return a String of the material's information
	 * @return String
	 */
	public String ToString();
	
}
