import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

/**
 * Main class for the program to run
 * @author catherinejarboe
 *
 */
public class MainSystem {

	private static Library library;
	private static Scanner scanner;
	private static boolean running = true;

/**
 * Main method for the program to run
 * @param args
 */
	public static void main(String[] args) {
		library = new Library();
		while (running) {
			library.readInputFile();
			library.greeting();
			scanner = new Scanner(System.in);
			boolean rightInput = false;
			while (rightInput == false) {
				try {
					Integer input = Integer.parseInt(scanner.nextLine());
					if (input == 1) {
						library.displayCollection();
						rightInput = true;
					} else if (input == 2) {
						library.checkOutMaterial();
						rightInput = true;
					} else if (input == 3) {
						running = false;
						rightInput = true;
						System.out.println("Program terminated!");
						break;
					} else if (input == null) {
						throw new EmptyStringException();
					} else {
						throw new InvalidInputException();
					}
				} catch (InvalidInputException in) {
					System.out.println("Invalid input! Input should be 1 or 2 or 3. Please try again: ");
				} catch (EmptyStringException es) {
					System.out.println("Empty input! Input must be an integer! Try again: ");
				} catch (NumberFormatException ex) {
					System.out.println("Invalid input! Input must be an integer! Try again: ");
				}
			}

		}
	}
}
