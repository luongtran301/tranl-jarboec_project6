import java.util.Calendar;
import java.util.GregorianCalendar;
/**
 * Class periodical is a type of material used by the library
 * @author catherinejarboe
 *
 */

public class Periodical extends Material{
	
	protected String volume;
	protected String issue;
	protected String subject;

	Periodical(String callNumber, String title, String volume, String issue, String subject) {
		super(callNumber, title);
		// TODO Auto-generated constructor stub
		this.volume = volume;
		this.issue = issue;
		this.subject = subject;
	}
	/**
	 * Returns the volume
	 * @return volume
	 */
	String getVolume() {
		return volume;
	}
	
	/**
	 * Returns the issue
	 * @return issue
	 */
	String getIssue() {
		return issue;
	}
	
	/**
	 * Returns the subject
	 * @return subject
	 */
	String getSubject() {
		return subject;
	}
	
	/**
	 * Sets the due date for the periodical
	 */
	void setDueDate() {
		// TODO Auto-generated method stub
		GregorianCalendar dateDue = new GregorianCalendar();
		dateDue.add(Calendar.DAY_OF_YEAR, 7);
		dueDate = String.format("%tD\n", dateDue);
	}
	
	/**
	 * Converts the periodical to a string for printing purposes
	 */
	public String toString() {
		String returnString = "";
		returnString += "\nPeriodical Title: " + this.getTitle()
						+ "\nCall Number: " + this.getCallNumber()	
						+ "\nVolume: " + this.getVolume()
						+ "\nIssue: " + this.getIssue()
						+ "\nSubject: " + this.getSubject();
		if(this.isCheckOut()) {
			returnString += "\nChecked Out: YES"
						  + "\nDate Out: " + this.getCheckOutDate()
						  + "\nDate Due: " + this.getDueDate();
		}
		else {
			returnString += "\nChecked Out: NO";
		}
		return returnString;
	}
	
}
