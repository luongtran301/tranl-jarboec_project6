
public interface ILibrary {
	
	/**
	 * This method would print out the menu for the program
	 */
	public void greeting();

	/**
	 * This method would read the sourceFile
	 * and generate the material array consists of Book and Periodical object
	 */
	public void readInputFile();
	
	/**
	 * This method would print out all the information about the objects in the material array
	 */
	public void displayCollection();
	
	/**
	 * This method would check out the material
	 * it would ask for the call number then look up for the matching material
	 */
	public void checkOutMaterial();
}

