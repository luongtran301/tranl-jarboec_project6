import java.util.Calendar;
import java.util.GregorianCalendar;
/**
 * A form of the material that the library class stores
 * @author catherinejarboe
 *
 */
public class Book extends Material{
	
	protected String author;
	protected String genre;

	Book(String callNumber, String title, String author, String genre) {
		super(callNumber, title);
		// TODO Auto-generated constructor stub
		this.author = author;
		this.genre = genre;
	}
	
	/**
	 * Returns the author of the book
	 * @return author
	 */
	String getAuthor() {
		return author;
	}
	
	/**
	 * Returns the genre of the book
	 * @return genre
	 */
	String getGenre() {
		return genre;
	}
	
	/**
	 * Sets the due date for the book
	 */
	void setDueDate() {
		// TODO Auto-generated method stub
		GregorianCalendar dateDue = new GregorianCalendar();
		dateDue.add(Calendar.DAY_OF_YEAR, 21);
		this.dueDate = String.format("%tD\n", dateDue);
	}

	@Override
	/**
	 * Returns the book as a string for printing purposes
	 */
	public String toString() {
		// TODO Auto-generated method stub
		
			String returnString = "";
			returnString += "\nBook Title: " + this.getTitle()
							+ "\nCall Number: " + this.getCallNumber()	
							+ "\nAuthor: " + this.getAuthor()
							+ "\nGenre: " + this.getGenre();
			if(this.isCheckOut()) {
				returnString += "\nChecked Out: YES"
							  + "\nDate Out: " + this.getCheckOutDate()
							  + "\nDate Due: " + this.getDueDate();
			}
			else {
				returnString += "\nChecked Out: NO";
		}
		return returnString;
	}
	
}
