import java.util.Calendar;
import java.util.GregorianCalendar;
/**
 * Generic class for materials that the library uses
 * @author catherinejarboe
 *
 */
public abstract class Material {
	
	protected String callNumber;
	protected boolean isCheckOut;
	protected String checkOutDate;
	protected String dueDate;
	protected String title;
	
	Material(String callNumber, String title){
		this.callNumber = callNumber;
		isCheckOut = false;
		this.title = title;
//		checkOutDate = this.setCheckOutDate();
//		dueDate = this.setDueDate()
	}
	/**
	 * Returns the title of the material
	 * @return title
	 */
	String getTitle() {
		return title;
	}
	
	/**
	 * Returns the call number of the material
	 * @return callNumber
	 */
	String getCallNumber() {
		return callNumber;
	}
	
	/**
	 * Returns the checked-out state of the material
	 * @return isCheckOut
	 */
	boolean isCheckOut() {
		return isCheckOut;
	}
	
	/**
	 * Sets the check-out state
	 */
	void setIsCheckOut() {
		isCheckOut = true;
	}
	
	/**
	 * Sets the check-out date which is different for each material
	 */
	void setCheckOutDate() {
		GregorianCalendar dateOut = new GregorianCalendar();
	    checkOutDate = String.format("%tD\n", dateOut);
	}
	
	/**
	 * Returns the check out date of the material
	 * @return checkOutDate
	 */
	String getCheckOutDate() {
		return checkOutDate;
	}
	
	/**
	 * Sets the due date
	 */
	abstract void setDueDate();
	
	/**
	 * Returns the due date
	 * @return dueDate
	 */
	String getDueDate() {
		return dueDate;
	}
	
	/**
	 * Returns the string version for printing purposes
	 */
	public String toString() { 
//		if(this.isCheckOut()) {
//			returnString += "\nChecked Out: YES"
//						  + "\nDate Out: " + this.getCheckOutDate()
//						  + "\nDate Due: " + this.getDueDate();
//		}
//		else {
//			returnString += "\nChecked Out: NO";
//		}
		String returnString = "";
		return returnString;
	}
}
